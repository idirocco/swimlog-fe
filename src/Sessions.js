import React from "react";
import Spinner from "./Spinner";
import Event from "./Event";

class Sessions extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="sessions">
        {this.props.sessions.map(session => {
          const events = session.events;
          return (
            <div key={session.id}>
              <h2>{session.name}</h2>
              <ul>
                {events.map(event => {
                  return (
                    <Event key={event.id} id={event.id} name={event.name} />
                  );
                })}
              </ul>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Sessions;
