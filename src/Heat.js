import React from "react";
import { render } from "react-dom";
import Timer from "./Timer";
import Chrono from "./Chrono/Chrono";
import {
  // MDBAutocomplete,
  MDBContainer,
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBInput,
  MDBIcon,
  MDBRow,
  MDBCol,
  MDBTable,
  MDBTableBody,
  MDBTableHead
} from "mdbreact";

class Heat extends React.Component {
  state = {
    users: [],
    distances: [],
    strokes: [],
    lanes: [],
    running: false
  };

  componentDidMount() {
    fetch(process.env.API_SERVER + "/api/run")
      .then(data => data.json())
      .then(
        data => {
          let runs,
            lanes = [];

          if (data) {
            if (Array.isArray(data)) {
              runs = data.slice(0, 3);
            } else {
              runs = [data];
            }
          } else {
            runs = [];
          }

          runs.map((run, i) => {
            lanes.push({
              index: i,
              id: run.id,
              user: run.user,
              distance: run.distance,
              stroke: run.stroke,
              time: run.time,
              chrono: {
                startTime: 0,
                elapsedTime: 0,
                running: false,
                interval: null,
                laps: []
              }
            });
          });

          this.setState({
            lanes: lanes
          });
        },
        error => {
          this.setState({
            error
          });
        }
      );
    fetch(process.env.API_SERVER + "/api/user")
      .then(data => data.json())
      .then(
        data => {
          let users;

          if (data) {
            if (Array.isArray(data)) {
              users = data;
            } else {
              users = [data];
            }
          } else {
            users = [];
          }

          this.setState({
            users: users
          });
        },
        error => {
          this.setState({
            error
          });
        }
      );
    fetch(process.env.API_SERVER + "/api/distance")
      .then(data => data.json())
      .then(
        data => {
          let distances;

          if (data) {
            if (Array.isArray(data)) {
              distances = data;
            } else {
              distances = [data];
            }
          } else {
            distances = [];
          }

          this.setState({
            distances: distances
          });
        },
        error => {
          this.setState({
            error
          });
        }
      );
    fetch(process.env.API_SERVER + "/api/stroke")
      .then(data => data.json())
      .then(
        data => {
          let strokes;

          if (data) {
            if (Array.isArray(data)) {
              strokes = data;
            } else {
              strokes = [data];
            }
          } else {
            strokes = [];
          }

          this.setState({
            strokes: strokes
          });
        },
        error => {
          this.setState({
            error
          });
        }
      );
  }

  handleStartClick = () => {
    let lanes = this.state.lanes;
    lanes.forEach((lane, i) => {
      this._startChrono(lanes[i].chrono, i);
    });
    this.setState({ lanes: lanes, running: true });
  };

  _startChrono = (chrono, i) => {
    chrono.startTime = Date.now();
    chrono.running = true;
    chrono.interval = setInterval(this.tick.bind(this, i), 10);
  }

  handleSplitClick = event => {
    let lanes = this.state.lanes;
    lanes[event.target.id].chrono.laps.push(this._createSplit(lanes[event.target.id].chrono));
    this.setState({ lanes: lanes });
  };

  handleStopClick = event => {
    this.doStop(event.target.id, true);
  };

  handleResetClick = () => {
    this._reset();
  };

  handleAddClick() {
    let user = document.getElementById("user").value;
    let distance = document.getElementById("distance").value;
    let stroke = document.getElementById("stroke").value;
    if (user == "" || distance == "" || stroke == "") {
      return false;
    } else {
      fetch(process.env.API_SERVER + "/api/run", {
        mode: "cors",
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          user_id: user,
          distance: distance,
          storke_id: stroke
        })
      })
        .then(data => data.json())
        .then(
          data => {
            console.log(data);
          },
          error => {
            this.setState({
              error
            });
          }
        );
    }
  }

  _createSplit = chrono => {
    let tsDiff = chrono.elapsedTime - chrono.startTime;
    chrono.laps.forEach(lapTs => {
      tsDiff -= lapTs;
    });
    return tsDiff;
  };

  doStop = (id, saveState) => {
    let lanes = this.state.lanes;
    lanes[id].chrono.laps.push(this._createSplit(lanes[id].chrono));
    clearInterval(lanes[id].chrono.interval);
    lanes[id].chrono.interval = null;
    lanes[id].chrono.running = false;
    if (typeof saveState !== 'undefined' && saveState == true){
      this.setState({ lanes: lanes });
    }
  };

  _stop() {
    let lanes = this.state.lanes;
    lanes.forEach((lane, i) => {
      this.doStop(lane.id, false);
    });
    this.setState({ lanes: lanes, running: false });
  }

  _reset() {
    let lanes = this.state.lanes;
    lanes.forEach((lane, i) => {
      clearInterval(lane.chrono.interval);
      lane.chrono.laps = [];
      lane.chrono.running = false;
      lane.chrono.interval = null;
      lane.chrono.startTime = 0;
      lane.chrono.elapsedTime = 0;
    });
    this.setState({ lanes: lanes, running:false });
  }

  tick = i => {
    let lanes = this.state.lanes;
    lanes[i].chrono.elapsedTime = Date.now();
    this.setState({ lanes: lanes });
  };

  render() {
    var { users, distances, strokes, lanes } = this.state;
    return (
      <MDBContainer>
        <div className="card">
          <div className="card-header">
            <MDBInput
              label="Heat name"
              group
              type="text"
              validate
              error="wrong"
              success="right"
            />
          </div>
          <div className="card-body">
            <MDBRow>
              <MDBCol>
                <select className="browser-default custom-select" id="user">
                  <option value="">User</option>
                  {users.map(user => {
                    return (
                      <option key={user.id} value={user.id}>
                        {user.name}
                      </option>
                    );
                  })}
                </select>
              </MDBCol>
              <MDBCol>
                <select className="browser-default custom-select" id="distance">
                  <option value="">Distance</option>
                  {distances.map(distance => {
                    return (
                      <option key={distance} value={distance}>
                        {distance}m
                      </option>
                    );
                  })}
                </select>
              </MDBCol>
              <MDBCol>
                <select className="browser-default custom-select" id="stroke">
                  <option value="">Stroke</option>
                  {strokes.map(stroke => {
                    return (
                      <option key={stroke.id} value={stroke.id}>
                        {stroke.name} ({stroke.abbr})
                      </option>
                    );
                  })}
                </select>
              </MDBCol>
              <MDBCol>
                <MDBBtn size="sm" onClick={this.handleAddClick}>
                  <MDBIcon icon="plus" className="ml-1" />
                </MDBBtn>
              </MDBCol>
            </MDBRow>
          </div>
        </div>
        <MDBTable>
          <MDBTableHead>
            <tr>
              <th />
              <th>Name</th>
              <th>Run</th>
              <th>Time</th>
              <th>
                <MDBBtn
                  color="success"
                  size="sm"
                  onClick={this.handleStartClick}
                >
                  <MDBIcon icon="clock" className="mr-1" />
                  Start
                </MDBBtn>
                <MDBBtn
                  color="warning"
                  size="sm"
                  onClick={this.handleResetClick}
                >
                  <MDBIcon icon="clock" className="mr-1" />
                  Reset
                </MDBBtn>
              </th>
            </tr>
          </MDBTableHead>
          <MDBTableBody>
            {lanes.map(lane => {
              return (
                <tr key={lane.id}>
                  <td>
                    <MDBIcon icon="arrows-alt-v" />
                  </td>
                  <td>{lane.user.name}</td>
                  <td>
                    {lane.distance}m {lane.stroke.name}
                  </td>
                  <td>
                  {lane.time} (best)
                    <Chrono
                      key={lane.id}
                      id={lane.index}
                      startTime={lane.chrono.startTime}
                      elapsedTime={lane.chrono.elapsedTime}
                      running={lane.chrono.running}
                      laps={lane.chrono.laps}
                      _handleStopClick={this.handleStopClick}
                      _handleSplitClick={this.handleSplitClick}
                    />
                  </td>
                </tr>
              );
            })}
          </MDBTableBody>
        </MDBTable>
      </MDBContainer>
    );
  }
}

export default Heat;
