import React from "react";

class Event extends React.Component {
  render() {
    const { id, name } = this.props;

    return (
      <li>
        {id} - {name}
      </li>
    );
  }
}

export default Event;
