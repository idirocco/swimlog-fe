import React from "react";
import Spinner from "./Spinner";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";

var strokeFormatter = (cell, row) => {
  let strokes = cell.split("(");
  return (
    <div>
      {strokes[0]} <span className="d-none d-lg-inline">({strokes[1]}</span>
    </div>
  );
};

const columns = [
  {
    dataField: "id",
    text: "ID",
    classes: "d-none",
    headerClasses: "d-none"
  },
  {
    text: "Name",
    dataField: "name",
    sort: true,
    filter: textFilter()
  },
  {
    text: "Stroke",
    dataField: "stroke",
    sort: true,
    filter: textFilter(),
    formatter: strokeFormatter
  },
  {
    text: "Time",
    dataField: "time",
    sort: true
  },
  {
    text: "Date",
    dataField: "date",
    sort: true,
    classes: "d-none d-lg-table-cell",
    headerClasses: "d-none d-lg-table-cell"
  },
  {
    text: "Age",
    dataField: "age",
    sort: true,
    classes: "d-none d-lg-table-cell",
    headerClasses: "d-none d-lg-table-cell"
  }
];

const rows = [];

const CaptionElement = () => <h1>Runs</h1>;

const defaultSorted = [
  {
    dataField: "date",
    order: "desc"
  }
];

const defaultPaginationOptions = {
  sizePerPage: 50
};

class Tests extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      loading: true,
      runs: []
    };
  }

  componentDidMount() {
    fetch(process.env.API_SERVER + "/api/run")
      .then(data => data.json())
      .then(
        data => {
          let _runs;

          if (data) {
            if (Array.isArray(data)) {
              _runs = data;
            } else {
              _runs = [data];
            }
          } else {
            _runs = [];
          }

          for (var i in _runs) {
            let row = {
              id: _runs[i].id,
              name: _runs[i].user.name,
              stroke: _runs[i].stroke.name + "(" + _runs[i].stroke.abbr + ")",
              date: new Intl.DateTimeFormat("en-US", {
                year: "numeric",
                month: "long",
                day: "2-digit"
              }).format(new Date(_runs[i].date)),
              age: _runs[i].user.category.name,
              time: _runs[i].time
            };

            rows.push(row);
          }

          this.setState({
            loading: false,
            runs: rows
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            loading: false,
            error
          });
        }
      );
  }
  render() {
    const { error, loading, runs } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (loading) {
      return <Spinner />;
    } else {
      return (
        <BootstrapTable
          caption={<CaptionElement />}
          bootstrap4
          keyField="id"
          data={runs}
          columns={columns}
          defaultSorted={defaultSorted}
          hover
          bordered={false}
          pagination={paginationFactory(defaultPaginationOptions)}
          filter={filterFactory()}
        />
      );
    }
  }
}

export default Tests;
