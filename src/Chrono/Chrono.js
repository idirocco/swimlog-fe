import React from "react";
import './Chrono.css';

class Chrono extends React.Component {
  constructor(props) {
    super(props);
  }

  zeroPad(value) {
    return value < 10 ? `0${value}` : value;
  }

  format(ms) {
      let millis = parseInt((ms%1000/10))
          , seconds = parseInt((ms/1000)%60)
          , minutes = parseInt((ms/(1000*60))%60)
      ;

      minutes = this.zeroPad(minutes)
      seconds = this.zeroPad(seconds);
      millis = this.zeroPad(millis);

      return minutes + ":" + seconds + ":" + millis;
  }

  render() {
    let { running, laps, startTime, elapsedTime } = this.props;
    let run = running !== false;
    return (
      <main className="chrono">
        <div className="display">
          <div className="time main">
            <div>{this.format(elapsedTime - startTime)}</div>
          </div>
          {laps.map((lap, i) => {
            return (
              <div className="time" key={i}>
                  {this.format(lap)}
              </div>
            );
          })}
          <div />
        </div>

        <div className="actions">
          <button
            className={
              "btn btn-default btn-sm split " + (false == run ? "disabled" : "")
            }
            onClick={this.props._handleSplitClick}
            id={this.props.id}
          >
            Lap
          </button>
          <button
            className={
              "btn btn-default btn-sm stop " + (false == run ? "disabled" : "")
            }
            onClick={this.props._handleStopClick}
            id={this.props.id}
          >
            Stop
          </button>
        </div>
      </main>
    );
  }
}

export default Chrono;
