import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "./App.css"

import React from "react";
import ReactDOM from "react-dom";
import { Router } from "@reach/router";
import Meets from "./Meets";
import Meet from "./Meet";
import Tests from "./Tests";
import Heat from "./Heat";
import Timer from "./Timer";
import Test from "./Test";

import {
  Container,
  Navbar,
  Nav,
} from "react-bootstrap";
import logo from "./images/logo.png";

class App extends React.Component {
  render() {
    return (
      <div>
        <Navbar bg="warning" variant="dark">
          <Container>
            <Navbar.Brand href="/">
              <img
                alt="SwimLog"
                src={logo}
                height="30"
                className="d-inline-block align-top"
              />
              {" SwimLog"}
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse
              id="basic-navbar-nav"
              className="justify-content-end"
            >
              <Nav.Link href="/">Meets</Nav.Link>
              <Nav.Link href="/tests">Tests</Nav.Link>
              <Nav.Link href="/heat">Heat</Nav.Link>
              <Nav.Link href="/timer">Timer</Nav.Link>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <div className="pt-3">
          <Router>
            <Meets path="/" />
            <Meet path="/meet/:id" />
            <Tests path="/tests" />
            <Heat path="/heat" />
            <Timer path="/timer" />
            <Test path="/test" />
          </Router>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
