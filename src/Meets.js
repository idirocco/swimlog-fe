import React from "react";
import { MDBListGroup, MDBListGroupItem, MDBContainer } from "mdbreact";
import MeetRow from "./MeetRow";
import Spinner from "./Spinner";

class Meets extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      loading: true,
      meets: []
    };
  }

  componentDidMount() {
    fetch(process.env.API_SERVER + "/api/meet")
      .then(data => data.json())
      .then(
        data => {
          let meets;

          if (data) {
            if (Array.isArray(data)) {
              meets = data;
            } else {
              meets = [data];
            }
          } else {
            meets = [];
          }

          this.setState({
            loading: false,
            meets: meets
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            loading: false,
            error
          });
        }
      );
  }
  render() {
    const { error, loading, meets } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (loading) {
      return <Spinner />;
    } else {
      return (
        <MDBContainer>
          <h1>Upcomming meets</h1>
          <MDBListGroup>
            {meets.map(meet => {
              return (
                <MeetRow
                  name={meet.name}
                  dateFrom={meet.dateFrom}
                  dateTo={meet.dateTo}
                  location={meet.location}
                  id={meet.id}
                  key={meet.id}
                />
              );
            })}
          </MDBListGroup>
        </MDBContainer>
      );
    }
  }
}

export default Meets;
