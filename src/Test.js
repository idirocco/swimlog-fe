import React from "react";
import Chrono from "./Chrono/Chrono";

const swimmers = [
  { id: 321, name: "Juan" },
  { id: 83, name: "Pedro" },
  { id: 13, name: "Jose" }
];

class Test extends React.Component {
  state = {
    chronos: []
  };

  componentDidMount() {
    let chronos = [];
    swimmers.forEach(function(swimmer, i) {
      chronos.push({
        id: swimmer.id,
        minutes: 0,
        seconds: 0,
        millis: 0,
        running: false,
        interval: null,
        splits: []
      });
    });

    this.setState({ chronos: chronos });
  }

  handleStartAllClick = () => {
    let chronos = this.state.chronos;
    for (var i = 0; i < chronos.length; i++) {
      chronos[i].interval = setInterval(this.tick.bind(this, i), 10);
      chronos[i].running = true;
    }
    this.setState({ chronos: chronos, running: true });
  };

  handleSplitClick = event => {
    let chronos = this.state.chronos;
    for (var i = 0; i < chronos.length; i++) {
      if (chronos[i].id == event.target.id) {
        chronos[i].splits.push(this._createSplit(chronos[i]));
      }
    }
    this.setState({ chronos: chronos });
  };

  handleStopClick = event => {
    this.doStop(event.target.id);
  };

  handleResetClick = () => {
    this.doStop();
    this._reset();
    this.updateAll(0, 0, 0);
  };

  _createSplit = chrono => {
    let split = {
      minutes: chrono.minutes,
      seconds: chrono.seconds,
      millis: chrono.millis
    };
    if (chrono.splits.length > 0) {
      let lastSplit = chrono.splits[chrono.splits.length - 1];
      let now = new Date();
      let time1 = new Date(
        now.getYear(),
        now.getMonth(),
        now.getDay(),
        0,
        lastSplit.minutes,
        lastSplit.seconds,
        lastSplit.millis
      );
      let time1ms = time1.getTime(time1);
      let time2 = new Date(
        now.getYear(),
        now.getMonth(),
        now.getDay(),
        0,
        chrono.minutes,
        chrono.seconds,
        chrono.millis
      );
      let time2ms = time2.getTime(time2);
      let diffms = time2ms - time1ms;
      let lapse = new Date(diffms);
      split.minutes = lapse.getMinutes();
      split.seconds = lapse.getSeconds();
      split.millis = lapse.getMilliseconds();
    }

    return split;
  };

  doStop = id => {
    let chronos = this.state.chronos;
    for (var i = 0; i < chronos.length; i++) {
      if (typeof id == "undefined" || chronos[i].id == id) {
        chronos[i].splits.push(this._createSplit(chronos[i]));
        clearInterval(chronos[i].interval);
        chronos[i].running = false;
      }
    }
    this.setState({ chronos: chronos });
  };

  tick(i) {
    let chrono = this.state.chronos[i];
    let millis = chrono.millis + 1;
    let seconds = chrono.seconds;
    let minutes = chrono.minutes;

    if (millis === 100) {
      millis = 0;
      seconds = seconds + 1;
    }

    if (seconds === 60) {
      millis = 0;
      seconds = 0;
      minutes = minutes + 1;
    }

    this.update(millis, seconds, minutes, i);
  }

  updateAll(millis, seconds, minutes) {
    let chronos = this.state.chronos;
    for (var i = 0; i < chronos.length; i++) {
      this.update(millis, seconds, minutes, i);
    }
  }

  _reset() {
    let chronos = this.state.chronos;
    for (var i = 0; i < this.state.chronos.length; i++) {
      chronos[i].splits = [];
    }
    this.setState({ chronos: chronos });
  }

  update(millis, seconds, minutes, i) {
    let chronos = this.state.chronos;
    chronos[i].millis = millis;
    chronos[i].seconds = seconds;
    chronos[i].minutes = minutes;
    this.setState({ chronos: chronos });
  }

  render() {
    var chronos = this.state.chronos;
    return (
      <div>
        <button
          className="btn btn-outline-success"
          onClick={this.handleStartAllClick}
        >
          Start
        </button>
        <button
          className="btn btn-outline-danger"
          onClick={this.handleResetClick}
        >
          Reset
        </button>
        {chronos.map(chrono => {
          return (
            <div key={chrono.id}>
              {chrono.name}
              <Chrono
                key={chrono.id}
                id={chrono.id}
                minutes={chrono.minutes}
                seconds={chrono.seconds}
                millis={chrono.millis}
                running={chrono.running}
                splits={chrono.splits}
                _handleStopClick={this.handleStopClick}
                _handleSplitClick={this.handleSplitClick}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

export default Test;
