import React from "react";
import { MDBListGroupItem } from "mdbreact";

class Meet extends React.Component {
  render() {
    const { id, name, dateFrom, location } = this.props;

    return (
      <MDBListGroupItem hover href={`/meet/${id}`}>
        <div className="d-flex w-100 justify-content-between">
          <h5 className="mb-1">{name}</h5>
        </div>
        <p className="mb-1">
          {dateFrom} - {location}
        </p>
      </MDBListGroupItem>
    );
  }
}

export default Meet;
