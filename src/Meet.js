import React from "react";
import { navigate } from "@reach/router";
import Spinner from "./Spinner";
import Sessions from "./Sessions";
import { Breadcrumb } from "react-bootstrap";

class Meet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      sessions: []
    };
  }

  componentDidMount() {
    fetch(process.env.API_SERVER + "/api/meet/" + this.props.id)
      .then(data => data.json())
      .then(data => {
        let meet;
        meet = data;
        if (Array.isArray(data)) {
          meet = data[0];
        }

        this.setState({
          name: meet.name,
          dateFrom: meet.dateFrom,
          dateTo: meet.dateTo,
          location: meet.location,
          sessions: meet.sessions,
          loading: false
        });
      })
      .catch(err => {
        this.setState({ error: err });
        // navigate("/");
      });
  }

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }

    const { id, name, dateFrom, dateTo, location, sessions } = this.state;

    return (
      <div>
        <Breadcrumb>
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="/meets">Meets</Breadcrumb.Item>
          <Breadcrumb.Item active>{name}</Breadcrumb.Item>
        </Breadcrumb>
        <h1>{name}</h1>
        <p>
          {dateFrom} / {dateTo} - {location}
        </p>
        <Sessions sessions={sessions} />
      </div>
    );
  }
}

export default Meet;
